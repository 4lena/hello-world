package com.example.helloworld;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class HelloControllerTest {
    @Test
    public void testHelloEndpoint() {
        HelloController helloController = new HelloController();
        String result = helloController.hello();
        assertEquals("Hello, world!", result);
    }
}
